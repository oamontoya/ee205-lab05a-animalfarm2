///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   10_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm {
   void Animal::printInfo() {
	   cout << "   Species = ["   << species              << "]" << endl;
	   cout << "   Gender = ["    << genderName( gender ) << "]" << endl;
   }

   string Animal::colorName (enum Color color) {
      switch(color){
         case BLACK:    return "Black";
         case WHITE:    return "White";
         case RED:      return "Red";
         case SILVER:   return "Silver";
         case YELLOW:   return "Yellow";
         case BROWN:    return "Brown";
      }
      return string("Unknown");
   };
   
   string Animal::genderName (enum Gender gender) {
      switch (gender) {
         case MALE:    return string("Male");      break;
         case FEMALE:  return string("Female");    break;
         case UNKNOWN: return string("Unknown");   break;
      }
      return string("Really, really Unknown");
   };
}
